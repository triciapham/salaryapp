package com.trang.salaryapp.service

import com.trang.salaryapp.entity.SalaryEntity
import com.trang.salaryapp.repository.SalaryRepository
import spock.lang.Specification

class SalaryServiceSpec extends Specification{
    SalaryService salaryService;
    SalaryRepository salaryRepository = Mock()

    def setup(){
         salaryService = new SalaryService(salaryRepository)
    }
    def "Test getSalary function"(){
        given:
            def employeeInputList = new ArrayList()

            def salaryEntity = new SalaryEntity()
            salaryEntity.setEmplid("testid1")
            salaryEntity.setSalary(1000)
            salaryEntity.setBonus(100)
            employeeInputList.add(salaryEntity)

            def salaryEntity1 = new SalaryEntity()
            salaryEntity1.setEmplid("testid2")
            salaryEntity1.setSalary(2000)
            salaryEntity1.setBonus(150)
            employeeInputList.add(salaryEntity1)

        when:
            def result= salaryService.getsalaryByEmplid(emplid)

        then:
            salaryRepository.findSalaryByEmplid(_) >> employeeInputList
            assert result.size() <= expectedSize
            assert result.get(0).getBonus() == expectedBonus

        where:
            emplid|expectedSize|expectedBonus
            "testid1"|2|100
            "testid0"|1|100
    }
}
