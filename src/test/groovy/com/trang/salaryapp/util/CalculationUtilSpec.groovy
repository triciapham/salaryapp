package com.trang.salaryapp.util

import spock.lang.Specification

class CalculationUtilSpec extends Specification{
    def "Test on Calculation Util"() {
    when:
            def result=CalculationUtil.combi(num1,num2,num3)
    then:
            assert expectResult == result
    where:
            num1|num2|num3|expectResult
            3|4|5|12
            11|2|3|10
            1|9|3|100
            15|3|12|300
    }
}
