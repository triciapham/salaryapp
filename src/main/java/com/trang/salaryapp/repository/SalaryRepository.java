package com.trang.salaryapp.repository;

import com.trang.salaryapp.entity.SalaryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface SalaryRepository extends JpaRepository<SalaryEntity, String> {
    @Query(value = "select * from compensation where id = ?1", nativeQuery = true)
    Collection<SalaryEntity> findSalaryByEmplid(String emplid);

}
