package com.trang.salaryapp.service;

import com.trang.salaryapp.model.SalaryResponse;
import com.trang.salaryapp.repository.SalaryRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SalaryService {
    private final SalaryRepository salaryRepository;

    public SalaryService(SalaryRepository salaryRepository) {
        this.salaryRepository = salaryRepository;
    }

    public SalaryResponse getsalary(String emplid)
    {

        SalaryResponse result = new  SalaryResponse();

        try {
            var item = Optional.ofNullable(salaryRepository.getById(emplid));
            result.setEmplid(item.get().getEmplid());
            result.setSalary(item.get().getSalary());
            result.setBonus(item.get().getBonus());

            return result;
        } catch(EntityNotFoundException e) {
            return result;
        }

    }

    public List<SalaryResponse> getsalaryByEmplid(String emplid)
    {
        List<SalaryResponse> list = new ArrayList<>();
        var items = salaryRepository.findSalaryByEmplid(emplid);
        items.forEach(item -> {
            SalaryResponse result = new SalaryResponse();
            result.setEmplid(item.getEmplid());
            result.setSalary(item.getSalary());
            result.setBonus(item.getBonus());

            list.add(result);
        });
        return list;
    }
}
