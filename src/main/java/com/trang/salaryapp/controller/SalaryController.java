package com.trang.salaryapp.controller;

import com.trang.salaryapp.model.SalaryResponse;
import com.trang.salaryapp.service.SalaryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController

public class SalaryController {
    private final SalaryService salaryService;
    public SalaryController(SalaryService salaryService) {
        this.salaryService = salaryService;
    }

    @GetMapping("/getSalary/{emplid}")
    public SalaryResponse getsalary(@PathVariable String emplid)  {
        return salaryService.getsalary(emplid) ;
    }

 @GetMapping("/getsalaryByEmplid/{emplid}")
 public List<SalaryResponse> getsalaryByEmplid(@PathVariable String emplid)  {
     return salaryService.getsalaryByEmplid(emplid) ;
 }
}
